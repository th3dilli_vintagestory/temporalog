using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Temporalog.Config;
using Vintagestory.API.Common;

namespace Temporalog.InfluxDB;

public class InfluxDbClient
{
    private readonly ILogger _logger;

    private readonly HttpClient _httpClient;

    private readonly string _writeEndpoint;
    
    private bool _isConnected;
    private bool _isReconnecting;
    private bool _isDisposed;

    public InfluxDbClient(TermporalogConfig config, ILogger logger)
    {
        _logger = logger;
        _writeEndpoint = $"write?org={config.Organization}&bucket={config.Bucket}";
        _httpClient = new HttpClient
        {
            BaseAddress = new Uri($"{config.Url}/api/v2/")
        };

        _httpClient.DefaultRequestHeaders.Add("Authorization", $"Token {config.Token}");
    }

    internal void Dispose()
    {
        _isDisposed = true;
        _httpClient.Dispose();
    }

    internal void WritePoint(PointData point, WritePrecision? precision)
    {
        if (!_isConnected || _isDisposed) return;
        Task.Run(async () =>
        {
            try
            {
                if (precision != null)
                {
                    var httpResponseMessage = await _httpClient.PostAsync(
                        $"{_writeEndpoint}&precision={precision.ToString()?.ToLower()}",
                        new StringContent(point.ToLineProtocol(), Encoding.UTF8, "application/json"));
                    if (!httpResponseMessage.IsSuccessStatusCode)
                    {
                        var response = await httpResponseMessage.Content.ReadAsStringAsync();
                        _logger.Warning($"[InfluxDB] {(int)httpResponseMessage.StatusCode} : {response}");
                        TryReconnect();
                    }
                }
                else
                {
                    var httpResponseMessage = await _httpClient.PostAsync(_writeEndpoint,
                        new StringContent(point.ToLineProtocol(), Encoding.UTF8, "application/json"));
                    if (!httpResponseMessage.IsSuccessStatusCode)
                    {
                        var response = await httpResponseMessage.Content.ReadAsStringAsync();
                        _logger.Warning($"[InfluxDB] {(int)httpResponseMessage.StatusCode} : {response}");
                        TryReconnect();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Warning($"[InfluxDB] {e}");
                TryReconnect();
            }
        });
    }

    internal void WritePoints(List<PointData> points, WritePrecision? precision)
    {
        if (!_isConnected || _isDisposed) return;
        Task.Run(async () =>
        {
            try
            {
                var sb = new StringBuilder();
                for (var i = 0; i < points.Count; i++)
                {
                    var point = points[i];
                    sb.Append(point.ToLineProtocol());
                    if (i <= points.Count - 1)
                    {
                        sb.Append("\n");
                    }
                }

                if (precision != null)
                {
                    var httpResponseMessage = await _httpClient.PostAsync(
                        $"{_writeEndpoint}&precision={precision.ToString()!.ToLower()}",
                        new StringContent(sb.ToString(), Encoding.UTF8, "application/json"));
                    if (!httpResponseMessage.IsSuccessStatusCode)
                    {
                        var response = await httpResponseMessage.Content.ReadAsStringAsync();
                        _logger.Warning($"[InfluxDB] {(int)httpResponseMessage.StatusCode} : {response}");
                        TryReconnect();
                    }
                }
                else
                {
                    var httpResponseMessage = await _httpClient.PostAsync(_writeEndpoint,
                        new StringContent(sb.ToString(), Encoding.UTF8, "application/json"));
                    if (!httpResponseMessage.IsSuccessStatusCode)
                    {
                        var response = await httpResponseMessage.Content.ReadAsStringAsync();
                        _logger.Warning($"[InfluxDB] {(int)httpResponseMessage.StatusCode} : {response}");
                        TryReconnect();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Warning($"[InfluxDB] {e}");
                TryReconnect();
            }
        });
    }

    public bool HasConnection()
    {
        try
        {
            var httpResponseMessage = _httpClient.GetAsync("orgs").GetAwaiter().GetResult();
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                _logger.Debug("Influxdb connected");
                _isConnected = true;
                return true;
            }

            _logger.Error(
                $"Error connecting to {_httpClient.BaseAddress}. {httpResponseMessage.StatusCode}");
            return false;
        }
        catch (Exception e)
        {
            _logger.Error(
                $"Could not connect to {_httpClient.BaseAddress}. {e.Message}");
            return false;
        }
    }

    public void TryReconnect()
    {
        if (_isReconnecting || _isDisposed) return;

        _isConnected = false;
        _logger.Notification(
            "Trying to reconnect in 10 sec");
        _isReconnecting = true;
        Task.Run(() =>
        {
            Thread.Sleep(10000);
            if (HasConnection())
            {
                _isReconnecting = false;
            }
            else
            {
                _isReconnecting = false;
                TryReconnect();
            }
        });
    }
}