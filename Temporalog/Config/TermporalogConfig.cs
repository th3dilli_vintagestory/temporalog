namespace Temporalog.Config;

public class TermporalogConfig
{
    public  string? Url;
    
    public  string? Token;
    
    public  string? Bucket;
    
    public  string? Organization;
    
    public  bool OverwriteLogTicks = false;

    public  bool EnableAdminLogging = false;
    
    public  int DataCollectInterval = 10000;
}