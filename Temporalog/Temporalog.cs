using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using HarmonyLib;
using Temporalog.Config;
using Temporalog.InfluxDB;
using Vintagestory.API.Common;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.Common;
using Vintagestory.Server;

namespace Temporalog;

internal class Temporalog : ModSystem
{
    private readonly Harmony _harmony;

    private long _writeDataListenerId;

    public static Temporalog? Instance { get; private set; }

    private const string HarmonyPatchKey = "Temporalog.Patch";

    private InfluxDbClient? _client;

    private ICoreServerAPI _sapi = null!;

    public static TermporalogConfig Config = null!;

    private ServerMain _server = null!;

    private Process? _vsProcess;

    private List<PointData> _data;
    private List<PointData>? _dataOnline;

    private const string ConfigFile = "TemporalogConfig.json";
    private int _errors;
    private int _errorsLastWrite = -1;

    private List<GameTickListener> _gameTickListenersEntity;
    private ConcurrentDictionary<long, DelayedCallback> _delayedCallbacksEntity;
    private List<GameTickListenerBlock> _gameTickListenersBlock;
    private List<DelayedCallbackBlock> _delayedCallbacksBlock;
    private Dictionary<BlockPos, DelayedCallbackBlock> _singleDelayedCallbacksBlock;

    public Temporalog()
    {
        _harmony = new Harmony(HarmonyPatchKey);
        _data = new List<PointData>();
        Instance = this;
    }
    
    public override void StartServerSide(ICoreServerAPI sapi)
    {
        _sapi = sapi;

        try
        {
            Config = _sapi.LoadModConfig<TermporalogConfig>(ConfigFile);

            if (Config == null)
            {
                Config = new TermporalogConfig();
                _sapi.StoreModConfig(Config, ConfigFile);

                Mod.Logger.Warning(
                    $"Config file {ConfigFile} was missing created new one at {Path.Combine(GamePaths.ModConfig, ConfigFile)}");
                Mod.Logger.Warning("Mod disabled");
                return;
            }
        }
        catch (Exception e)
        {
            Mod.Logger.Error(e);
            Mod.Logger.Warning("Mod disabled");
            return;
        }

        _server = (ServerMain)_sapi.World;
        
        
        _vsProcess = Process.GetCurrentProcess();

        _client = new InfluxDbClient(Config, Mod.Logger);
        if (!_client.HasConnection())
        {
            _client.TryReconnect();
        }

        PatchFrameProfilerUtil.Patch(_harmony);
        if(Config.EnableAdminLogging)
        {
            PatchAdminLogging.Patch(_harmony);
            _sapi.Event.DidPlaceBlock += OnDidPlaceBlock;
            _sapi.Event.DidBreakBlock += OnDidBreakBlock;
        }

        _sapi.Logger.EntryAdded += LogEntryAdded;
        _sapi.Event.PlayerDeath += PlayerDeath;

        _gameTickListenersEntity = (List<GameTickListener>)_server.EventManager.GetType()
            .GetField("GameTickListenersEntity", BindingFlags.Instance | BindingFlags.NonPublic)!.GetValue(_server.EventManager)!;
        
        _delayedCallbacksEntity = (ConcurrentDictionary<long, DelayedCallback>)_server.EventManager.GetType()
            .GetField("DelayedCallbacksEntity", BindingFlags.Instance | BindingFlags.NonPublic)!.GetValue(_server.EventManager)!;
        
        _gameTickListenersBlock = (List<GameTickListenerBlock>)_server.EventManager.GetType()
            .GetField("GameTickListenersBlock", BindingFlags.Instance | BindingFlags.NonPublic)!.GetValue(_server.EventManager)!;
        
        _delayedCallbacksBlock = (List<DelayedCallbackBlock>)_server.EventManager.GetType()
            .GetField("DelayedCallbacksBlock", BindingFlags.Instance | BindingFlags.NonPublic)!.GetValue(_server.EventManager)!;
        
        _singleDelayedCallbacksBlock = (Dictionary<BlockPos, DelayedCallbackBlock>)_server.EventManager.GetType()
            .GetField("SingleDelayedCallbacksBlock", BindingFlags.Instance | BindingFlags.NonPublic)!
            .GetValue(_server.EventManager)!;

        _writeDataListenerId = _sapi.Event.RegisterGameTickListener(WriteOnline, 10000);
        _writeDataListenerId = _sapi.Event.RegisterGameTickListener(WriteData, Config.DataCollectInterval);
    }

    private void OnDidBreakBlock(IServerPlayer byPlayer, int oldBlockId, BlockSelection blockSel)
    {
        if (byPlayer.WorldData.CurrentGameMode != EnumGameMode.Creative) return;
        var pointData = PointData.Measurement("playerlogcrbreak").Tag("player", byPlayer.PlayerName.ToLower())
            .Tag("playerUID", byPlayer.PlayerUID).Tag("position", blockSel.Position.ToString()).Field("value",
                $"{_sapi.World.Blocks[oldBlockId].Code} {blockSel.Position}");
        WritePoint(pointData, WritePrecision.Ms);
    }

    private void OnDidPlaceBlock(IServerPlayer byPlayer, int oldBlockId, BlockSelection blockSel,
        ItemStack withItemStack)
    {
        if (byPlayer.WorldData.CurrentGameMode != EnumGameMode.Creative) return;
        var pointData = PointData.Measurement("playerlogcrplace").Tag("player", byPlayer.PlayerName.ToLower())
            .Tag("playerUID", byPlayer.PlayerUID).Tag("position", blockSel.Position.ToString()).Field("value",
                $"{withItemStack.Collectible?.Code} {blockSel.Position}");
        WritePoint(pointData, WritePrecision.Ms);
    }

    private void LogEntryAdded(EnumLogType logType, string message, object[] args)
    {
        switch (logType)
        {
            case EnumLogType.Chat:
                break;
            case EnumLogType.Event:
                break;
            case EnumLogType.StoryEvent:
                break;
            case EnumLogType.Build:
                break;
            case EnumLogType.VerboseDebug:
                break;
            case EnumLogType.Debug:
                break;
            case EnumLogType.Notification:
                break;
            case EnumLogType.Warning:
            {
                var msg = string.Format(message, args);
                if (msg.Contains("Server overloaded"))
                {
                    WritePoint(PointData.Measurement("overloadwarnings").Field("value", msg));
                }
                else
                {
                    WritePoint(PointData.Measurement("warnings").Field("value", msg));
                }

                break;
            }
            case EnumLogType.Error:
            case EnumLogType.Fatal:
            {
                _errors++;
                WritePoint(PointData.Measurement("errors").Field("value", string.Format(message, args)));
                break;
            }
            case EnumLogType.Audit:
                break;
        }
    }

    private void WriteData(float t1)
    {
        _data = new List<PointData>();

        var activeEntities =
            _sapi.World.LoadedEntities.Count(loadedEntity => loadedEntity.Value.IsTracked != 0);
        _data.Add(PointData.Measurement("entitiesActive").Field("value", activeEntities));

        var statsCollection =
            _server.StatsCollector[GameMath.Mod(_server.StatsCollectorIndex - 1, _server.StatsCollector.Length)];
        if (statsCollection.ticksTotal > 0)
        {
            _data.Add(PointData.Measurement("l2avgticktime").Field("value",
                (double)statsCollection.tickTimeTotal / statsCollection.ticksTotal));
            _data.Add(PointData.Measurement("l2stickspersec").Field("value", statsCollection.ticksTotal / 2.0));
        }

        _data.Add(PointData.Measurement("packetspresec").Field("value", statsCollection.statTotalPackets / 2.0));
        _data.Add(PointData.Measurement("kilobytespersec").Field("value",
            decimal.Round((decimal)(statsCollection.statTotalPacketsLength / 2048.0), 2,
                MidpointRounding.AwayFromZero)));

        _data.Add(PointData.Measurement("packetspresecudp").Field("value", statsCollection.statTotalUdpPackets / 2.0));
        _data.Add(PointData.Measurement("kilobytespersecudp").Field("value",
            decimal.Round((decimal)(statsCollection.statTotalUdpPacketsLength / 2048.0), 2,
                MidpointRounding.AwayFromZero)));

        _vsProcess?.Refresh();
        var totalMemory = _vsProcess?.PrivateMemorySize64 / 1048576;
        var managedMemory = GC.GetTotalMemory(false) / 1048576;
        
        _data.Add(PointData.Measurement("memory").Field("value", totalMemory ?? 0));
        _data.Add(PointData.Measurement("memoryManaged").Field("value", managedMemory));

        _data.Add(PointData.Measurement("threads").Field("value", _vsProcess?.Threads.Count ?? 0));

        _data.Add(PointData.Measurement("chunks").Field("value", _sapi.World.LoadedChunkIndices.Length));

        _data.Add(PointData.Measurement("entities").Field("value", _sapi.World.LoadedEntities.Count));

        _data.Add(PointData.Measurement("generatingChunks")
            .Field("value", _sapi.WorldManager.CurrentGeneratingChunkCount));
        try
        {
            _data.Add(PointData.Measurement("callbacksEntity").Field("value", _delayedCallbacksEntity.Values.Count(x => x != null)));
            _data.Add(PointData.Measurement("callbacksBlock").Field("value", _delayedCallbacksBlock.Count(x => x != null)));
            _data.Add(PointData.Measurement("listenersEntity").Field("value", _gameTickListenersEntity.Count(x => x != null)));
            _data.Add(PointData.Measurement("listenersBlock").Field("value", _gameTickListenersBlock.Count(x => x != null)));
            _data.Add(PointData.Measurement("singleCallbacksBlock").Field("value", _singleDelayedCallbacksBlock.Values.Count(x => x != null)));
        }
        catch (Exception e)
        {
            Mod.Logger.Warning("Error writing tick listeners, maybe due to incompatible mod and game version");
            Mod.Logger.Warning(e);
        }

        
        if (_errors != _errorsLastWrite)
        {
            _errorsLastWrite = _errors;
            _data.Add(PointData.Measurement("errorsNum").Field("value", _errors));
        }
        
        WritePoints(_data);
    }

    private void WriteOnline(float t1)
    {
        _dataOnline = new List<PointData>();

        foreach (var player in _sapi.World.AllOnlinePlayers.Cast<IServerPlayer>())
        {
            if (player.ConnectionState == EnumClientState.Playing)
            {
                _dataOnline.Add(PointData.Measurement("online").Tag("player", player.PlayerName.ToLower())
                    .Field("value", player.Ping));
            }
        }

        var connected = _server.Clients.Count(x => x.Value.State is EnumClientState.Connected or EnumClientState.Connecting or EnumClientState.Playing);
        var inQueue = _server.Clients.Count(x => x.Value.State == EnumClientState.Queued);
        
        _dataOnline.Add(PointData.Measurement("clients").Field("value", connected));
        _dataOnline.Add(PointData.Measurement("clientsQueue").Field("value", inQueue));
        WritePoints(_dataOnline);
    }

    public void WritePoints(List<PointData> data, WritePrecision? precision = null)
    {
        _client?.WritePoints(data, precision);
    }

    public void WritePoint(PointData data, WritePrecision? precision = null)
    {
        _client?.WritePoint(data, precision);
    }

    private void PlayerDeath(IServerPlayer byplayer, DamageSource damagesource)
    {
        var causeEntity = damagesource.GetCauseEntity();
        var playerName = causeEntity is EntityPlayer pl ? $"({pl.Player.PlayerName})" : causeEntity?.GetName();
        var source = $"{causeEntity?.GetType().Name} {playerName} [{damagesource.Type}] : {damagesource.Source}";
        WritePoint(PointData.Measurement("deaths").Tag("player", byplayer.PlayerName.ToLower())
            .Field("value", source));
    }

    public override void Dispose()
    {
        if (_client != null)
        {
            Mod.Logger.EntryAdded -= LogEntryAdded;

            _sapi.Event.UnregisterGameTickListener(_writeDataListenerId);

            _client.Dispose();
        }

        _harmony.UnpatchAll(HarmonyPatchKey);
    }
}